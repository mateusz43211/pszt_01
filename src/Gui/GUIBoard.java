package Gui;

import Logic.*;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Enum type of move moment
 */
enum MoveMoment
{
    START_MOMENT,
    PAWN_SELECTED,
    PAWN_MOVED
}

/**
 * Window with board
 */
public class GUIBoard extends JFrame implements MouseListener, OnLogicListener, OnMoveListener {
    private byte PositionStartX, PositionStartY, PositionMoveX, PositionMoveY, PositionShotX, PositionShotY;
    private final IBoardInfo boardInfo;
    private LogicHolder logicHolder;
    private MoveMoment Turn;
    private Graphics2D g2;
    private Configuration configuration;
    private boolean isShooted = false;

    /**
     * Constructor
     * @param configuration configuration of game
     */
    public GUIBoard(Configuration configuration) {
        super("Gra w Amazonki");
        this.configuration = configuration;
        logicHolder = new LogicHolder(this, configuration);
        boardInfo = logicHolder;
        setSize(808,837);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int x = (int)d.getWidth();
        int y = (int)d.getHeight();
        setLocation((x-808)/2, (y-837)/2);
        Turn = MoveMoment.START_MOMENT;
        addMouseListener(this);
        setResizable(false);
        repaint();
        if(configuration.getGamePlayOption()== Configuration.GamePlayOption.AI_VS_AI)
            logicHolder.startAIGame();
    }

    /**
     * Function paint the board
     * @param g graphics
     */
    public void paint(Graphics g)
    {
        URL imageURL = getClass().getResource("/resources/" + "board.png");
        BufferedImage bi;
        try{

            bi = ImageIO.read(imageURL);
            g2 =(Graphics2D)bi.getGraphics();
            byte[][] blackQueens = boardInfo.getBlackQueens();
            for (byte[] queen : blackQueens) {
                printPawn('b', queen[1], queen[0]);
            }

            byte[][] whiteQueens = boardInfo.getWhiteQueens();
            for (byte[] queen : whiteQueens) {
                printPawn('w', queen[1], queen[0]);
            }

            ArrayList<byte[]> arrowPos = boardInfo.getArrowPosition();
            for (byte[] pos : arrowPos) {
                printArrow(pos[1], pos[0]);
            }

            if(Turn == MoveMoment.PAWN_SELECTED) {
                ArrayList<byte[]> possibleMovesForQueen = boardInfo.getPossibleMovesForQueen(PositionStartX, PositionStartY);
                for (byte[] pos : possibleMovesForQueen) {
                    printBacklight(pos[1], pos[0]);
                }
            }
            if(Turn == MoveMoment.PAWN_MOVED) {
                if(!isShooted) {
                    ArrayList<byte[]> possibleArrowShootsForQueen = boardInfo.getPossibleArrowShootsForQueen(PositionStartX, PositionStartY, PositionMoveX, PositionMoveY);
                    for (byte[] pos : possibleArrowShootsForQueen) {
                        printBacklight(pos[1], pos[0]);
                    }
                }
                else {
                    printArrow(PositionShotY, PositionShotX);
                    Color color;
                    if((PositionStartX%2==1 && PositionStartY%2==1) || (PositionStartX%2==0 && PositionStartY%2==0))
                        color = new Color(185,122, 87);
                    else
                        color = new Color(239,228, 176);
                    printRect(color, PositionStartY, PositionStartX);
                }
                printPawn('w', PositionMoveY, PositionMoveX);

            }

            g.drawImage(bi,4,33,this);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Function paint pawns
     * @param color color of pawn
     * @param locX x localization of pawn
     * @param locY y localization of pawn
     */
    private void printPawn(char color, int locX, int locY)
    {
        if(color == 'w')
        {
            g2.setColor(Color.white);
            g2.fillOval(7+locX*80, 7+locY*80, 65, 65);
        }
        if(color == 'b')
        {
            g2.setColor(Color.black);
            g2.fillOval(7+locX*80, 7+locY*80, 65, 65);
        }
    }

    /**
     * Function paint arrows
     * @param locX x localization of arrow
     * @param locY y localization of arrow
     */
    private void printArrow(int locX, int locY)
    {
        g2.setColor(Color.red);
        g2.fillOval(7+locX*80, 7+locY*80, 65, 65);
    }

    /**
     * Function paint back light
     * @param locX x localization of back light
     * @param locY y localization of back light
     */
    private void printBacklight(int locX, int locY)
    {
        g2.setColor(Color.LIGHT_GRAY);
        g2.fillRect(3+locX*80, 3+locY*80, 74, 74);
    }

    /**
     * Function paint rectangle
     * @param color color of board box
     * @param locX x localization of rectangle
     * @param locY y localization of rectangle
     */
    private void printRect(Color color, int locX, int locY)
    {
        g2.setColor(color);
        g2.fillRect(3+locX*80, 3+locY*80, 74, 74);
    }

    /**
     * Configure mouse click events
     * @param e event
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (configuration.getGamePlayOption() != Configuration.GamePlayOption.AI_VS_AI) {
            if(e.getX()>=4 && e.getX()<=804 && e.getY()>=33 && e.getY()<=833) {
                switch (Turn) {
                    case START_MOMENT:
                        System.out.println("1. klik");
                        isShooted = false;
                        int locY = e.getX() - 4;
                        int locX = e.getY() - 33;
                        PositionStartX = (byte) (locX / 80);
                        PositionStartY = (byte) (locY / 80);
                        if (boardInfo.isPositionByRoleOccupied(Role.WHITE, PositionStartX, PositionStartY)) {
                            Turn = MoveMoment.PAWN_SELECTED;
                        }
                        break;
                    case PAWN_SELECTED:
                        System.out.println("2. klik");
                        locY = e.getX() - 4;
                        locX = e.getY() - 33;
                        PositionMoveX = (byte) (locX / 80);
                        PositionMoveY = (byte) (locY / 80);
                        if(boardInfo.isPossibleMoves(boardInfo.getPossibleMovesForQueen(PositionStartX, PositionStartY), PositionMoveX, PositionMoveY)) {
                            Turn = MoveMoment.PAWN_MOVED;
                        }
                        break;
                    case PAWN_MOVED:
                        System.out.println("3. klik");
                        locY = e.getX() - 4;
                        locX = e.getY() - 33;
                        PositionShotX = (byte) (locX / 80);
                        PositionShotY = (byte) (locY / 80);
                        if (boardInfo.isPossibleMoves(boardInfo.getPossibleArrowShootsForQueen(PositionStartX, PositionStartY, PositionMoveX, PositionMoveY), PositionShotX, PositionShotY)) {
                            isShooted = true;
                            repaint();
                            SwingUtilities.invokeLater(()->makeMove());
                        }
                        break;
                }
            }
            repaint();
        }
    }

    public void makeMove() {
        onMove(PositionStartX, PositionStartY, PositionMoveX, PositionMoveY, PositionShotX, PositionShotY);
        Turn = MoveMoment.START_MOMENT;
    }

    /**
     * Configure mouse click events
     * @param e event
     */
    @Override
    public void mousePressed(MouseEvent e) {

    }

    /**
     * Configure mouse click events
     * @param e event
     */
    @Override
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Configure mouse click events
     * @param e event
     */
    @Override
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * Configure mouse click events
     * @param e event
     */
    @Override
    public void mouseExited(MouseEvent e) {

    }

    /**
     * Function transfer information about move with gui to logic
     * @param qr start x position of pawn
     * @param qc start y position of pawn
     * @param qfr end x position of pawn
     * @param qfc end y position of pawn
     * @param ar x position of arrow
     * @param ac y position of arrow
     */
    @Override
    public void onMove(byte qr, byte qc, byte qfr, byte qfc, byte ar, byte ac) {
        logicHolder.makeMove(qr, qc, qfr, qfc, ar, ac);
    }

    /**
     * Function transfer information about move with logic to gui
     * @param move actual move
     */
    @Override
    public void onPawnMove(Move move) {
        if (SwingUtilities.isEventDispatchThread()) {
            if(configuration.getGamePlayOption() == Configuration.GamePlayOption.PLAYER_VS_AI) {
                YourTurnWindow window = new YourTurnWindow();
                window.setVisible(true);
            }
            repaint();
        }
        else {
            SwingUtilities.invokeLater(()->repaint());
        }
    }

    /**
     * Function transfer information about game over to gui and show game over window
     * @param role role of winner (white or black)
     */
    @Override
    public void onGameOver(Role role) {
        GameOver gameOverWindow = new GameOver(role);
        gameOverWindow.setVisible(true);
    }
}
