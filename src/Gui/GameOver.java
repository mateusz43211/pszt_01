package Gui;

import Logic.Role;

import javax.swing.*;
import java.awt.*;

/**
 * Game over window - show information about winner
 */
public class GameOver extends JFrame {
    /**
     * Constructor
     * @param role role winner player (white or black)
     */
    public GameOver(Role role) {
        super("Koniec gry");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 150);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int x = (int) d.getWidth();
        int y = (int) d.getHeight();
        setLocation((x - 300) / 2, (y - 150) / 2);
        setResizable(false);

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);

        String winner;

        if(role == Role.WHITE)
            winner = "biały !!!";
        else
            winner = "czarny !!!";

        JLabel text = new JLabel ("Wygrał " + winner);
        text.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        text.setVerticalAlignment(SwingConstants.CENTER);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        topPanel.add(text);
    }
}
