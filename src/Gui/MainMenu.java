package Gui;

import Logic.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Main window - user can choose game with AI or gam AI vs AI
 *
 * @author Karol
 */
public class MainMenu extends JFrame implements ActionListener {

    private JPanel panel;
    private JButton button1;
    private JButton button2;

    /**
     * Constructor
     */
    public  MainMenu() {
        super("Amazonki");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300,250);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int x = (int)d.getWidth();
        int y = (int)d.getHeight();
        setLocation((x-300)/2, (y-300)/2);
        setResizable(false);

        panel = new JPanel();
        panel.setLayout(null);
        add(panel);

        JLabel text = new JLabel ("Witamy w grze!");
        text.setBounds(77,30,200,25);
        text.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        panel.add(text);

//        button1 = new JButton("Tryb dwuosobowy");
//        button1.setBounds(40, 80, 200, 25);
//        button1.addActionListener(this);
//        panel.add(button1);

        button1 = new JButton("Tryb jednoosobowy");
        button1.setBounds(47, 80, 200, 40);
        button1.addActionListener(this);
        panel.add(button1);

        button2 = new JButton("Rozgrywka CPU z CPU");
        button2.setBounds(47, 140, 200, 40);
        button2.addActionListener(this);
        panel.add(button2);
    }

    /**
     * Configure button action events
     * @param e action event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
//        if (e.getSource() == button1) {
//            Configuration configuration = new Configuration();
//            configuration.setGamePlayOption(Configuration.GamePlayOption.AI_VS_AI);
//            configuration.setHeuristicOption(Configuration.HeuristicOption.TERRITORY);
//            configuration.setNumberOfChildForState(20);
//            configuration.setSearchOption(Configuration.SearchOption.SEARCH_TIME_LIMIT);
//            long timeLimit = 10000000000L;
//            configuration.setSearchValue(timeLimit);
//            GUIBoard gui = new GUIBoard(configuration);
//            gui.setVisible(true);
//            gui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        }
        if (e.getSource() == button1) {
            Configuration configuration = new Configuration();
            configuration.setGamePlayOption(Configuration.GamePlayOption.PLAYER_VS_AI);
            Options options1 = new Options(configuration);
            options1.setVisible(true);
        }
        else if (e.getSource() == button2) {
            Configuration configuration = new Configuration();
            configuration.setGamePlayOption(Configuration.GamePlayOption.AI_VS_AI);
            Options options2 = new Options(configuration);
            options2.setVisible(true);
        }
    }
}
