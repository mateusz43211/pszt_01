package Gui;

/**
 * Interface Move Lister transfer information about move with gui to logic
 */
public interface OnMoveListener {
    void onMove(byte qr, byte qc, byte qfr, byte qfc, byte ar, byte ac);
}
