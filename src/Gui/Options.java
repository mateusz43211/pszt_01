package Gui;

import Logic.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Option window - user can choose options of the game
 *
 * @author Karol
 */
public class Options extends JFrame implements ActionListener {

    private Configuration configuration;
    private JPanel panel1, panel2, panel3;
    private ButtonGroup bgHeuristic, bgQualityGame;
    private JRadioButton rbMobility, rbTerritory, rbTerritoryMobility, rbDepth, rbTime;
    private JSlider slDepth, slTime, slChildren;

    /**
     * Constructor
     * @param configuration configuration of game
     */
    public Options(Configuration configuration) {
        super("Opcje");
        this.configuration = configuration;

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(300,360);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int x = (int)d.getWidth();
        int y = (int)d.getHeight();
        setLocation((x-300)/2, (y-360)/2);
        setResizable(false);

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
        getContentPane().add(topPanel);

        createPanel1();
        createPanel2();
        createPanel3();

        topPanel.add(panel1);
        topPanel.add(panel2);
        topPanel.add(panel3);

        JButton btAccept = new JButton("Akceptuj");
        btAccept.setAlignmentX(Component.CENTER_ALIGNMENT);
        btAccept.addActionListener(this);
        topPanel.add(btAccept);


    }

    /**
     * Function create first JPanel
     */
    public void createPanel1(){
        panel1 = new JPanel();
        panel1.setLayout(new GridLayout(3, 1));
        panel1.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Wybierz heurystykę?"));
        panel1.setMaximumSize(new Dimension(300,100));
        panel1.setMinimumSize(new Dimension(300,100));
        bgHeuristic = new ButtonGroup();
        rbMobility = new JRadioButton("Mobility", true);
        bgHeuristic.add(rbMobility);
        panel1.add(rbMobility);
        rbTerritory = new JRadioButton("Territory", false);
        bgHeuristic.add(rbTerritory);
        panel1.add(rbTerritory);
        rbTerritoryMobility = new JRadioButton("Territory-Mobility", false);
        bgHeuristic.add(rbTerritoryMobility);
        panel1.add(rbTerritoryMobility);
    }

    /**
     * Function create second JPanel
     */
    public void createPanel2(){
        panel2 = new JPanel();
        panel2.setLayout(new GridLayout(4, 1));
        panel2.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Ustaw jakość poziomu gry po"));
        panel2.setMaximumSize(new Dimension(300,133));
        panel2.setMinimumSize(new Dimension(300,133));
        bgQualityGame = new ButtonGroup();
        rbDepth = new JRadioButton("głębokości poszukiwań", true);
        bgQualityGame.add(rbDepth);
        panel2.add(rbDepth);
        slDepth = new JSlider(2,6,3);
        slDepth.setMajorTickSpacing(1);
        slDepth.setMinorTickSpacing(1);
        slDepth.setPaintLabels(true);
        panel2.add(slDepth);
        rbTime = new JRadioButton("czasie przeszukiwania", false);
        bgQualityGame.add(rbTime);
        panel2.add(rbTime);
        slTime = new JSlider(1,10,5);
        slTime.setMajorTickSpacing(1);
        slTime.setMinorTickSpacing(1);
        slTime.setPaintLabels(true);
        panel2.add(slTime);
    }

    /**
     * Function create third JPanel
     */
    public void createPanel3() {
        panel3 = new JPanel();
        panel3.setLayout(new GridLayout(1, 1));
        panel3.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Ustaw ilość potomków w następnym pokoleniu"));
        panel3.setMaximumSize(new Dimension(300,45));
        panel3.setMinimumSize(new Dimension(300,45));
        slChildren = new JSlider(4,30,20);
        slChildren.setMajorTickSpacing(2);
        slChildren.setMinorTickSpacing(1);
        slChildren.setPaintLabels(true);
        panel3.add(slChildren);
    }

    /**
     * Configure window action events
     * @param e action event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (rbMobility.isSelected())
            configuration.setHeuristicOption(Configuration.HeuristicOption.MOBILITY);
        else if (rbTerritory.isSelected())
            configuration.setHeuristicOption(Configuration.HeuristicOption.TERRITORY);
        else if (rbTerritoryMobility.isSelected())
            configuration.setHeuristicOption(Configuration.HeuristicOption.TERRITORY_MOBILITY);
        if (rbDepth.isSelected()) {
            configuration.setSearchOption(Configuration.SearchOption.SEARCH_MAX_DEPTH);
            configuration.setNumberOfChildForState(slChildren.getValue());
            configuration.setSearchValue(slDepth.getValue());
        }
        else if (rbTime.isSelected()) {
            configuration.setSearchOption(Configuration.SearchOption.SEARCH_TIME_LIMIT);
            configuration.setNumberOfChildForState(slChildren.getValue());
            long timeLimit = slTime.getValue();
            timeLimit = timeLimit * 1000000000L;
            System.out.println(timeLimit);
            configuration.setSearchValue(timeLimit);
        }
        GUIBoard gui = new GUIBoard(configuration);
        gui.setVisible(true);
        gui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
}