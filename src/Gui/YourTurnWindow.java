package Gui;

import javax.swing.*;
import java.awt.*;

/**
 * Your turn window - show information about players turn
 */
public class YourTurnWindow extends JFrame {
    /**
     * Constructor
     */
    public YourTurnWindow() {
        super("Twój ruch");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(300, 150);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        int x = (int) d.getWidth();
        int y = (int) d.getHeight();
        setLocation((x - 300) / 2, (y - 150) / 2);
        setResizable(false);

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);

        JLabel text = new JLabel ("Twój ruch!");
        text.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        text.setVerticalAlignment(SwingConstants.CENTER);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        topPanel.add(text);
        }
}
