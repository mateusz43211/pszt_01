package Logic;

import Logic.Configuration.HeuristicOption;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * holds logic for game (positions of pawns, board)
 * holds methods to evaluate game with specified heuristic
 */
public class Board implements GameState {
    static final byte NUM_ROWS = 10;
    static final byte NUM_COLS = 10;

    static final byte[][] dirs = {{1, 1}, {-1, 1}, {1, -1}, {-1, -1}, {0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    private static final byte FREE = 0;
    private static final byte WHITE_QUEEN = 1;
    private static final byte BLACK_QUEEN = 2;
    private static final byte ARROW = 3;
    private byte[][] whiteQueens;
    private byte[][] blackQueens;

    protected byte[][] board = new byte[NUM_ROWS][NUM_COLS];

    private EvaluationFunction evaluationFunction;
    private SuccesorFunction succesorFunction = new SuccesorFunction();

    private byte markedPositions = 0;

    private HeuristicOption heuristicOption;
    Role currentPlayer = Role.WHITE;

    private Board() {

    }

    /**
     * Creates board with specified heuristic option
     * @param option
     */
    Board(HeuristicOption option) {

        this.heuristicOption = option;
        switch (option) {
            case MOBILITY:
                evaluationFunction = new MobilityEvaluationFunction();
                break;
            case TERRITORY:
                evaluationFunction = new TerritoryEvaluationFunction();
                break;
            case TERRITORY_MOBILITY:
                evaluationFunction = new TerritoryMobilityEvaluationFunction();
                break;
        }
        // initialize board
        for (byte r = 0; r < NUM_ROWS; ++r) {
            for (byte c = 0; c < NUM_COLS; ++c) {
                board[r][c] = FREE;
            }
        }

        whiteQueens = new byte[4][2];
        blackQueens = new byte[4][2];

        whiteQueens[0][0] = 3;
        whiteQueens[0][1] = 0;
        whiteQueens[1][0] = 0;
        whiteQueens[1][1] = 3;
        whiteQueens[2][0] = 0;
        whiteQueens[2][1] = 6;
        whiteQueens[3][0] = 3;
        whiteQueens[3][1] = 9;

        blackQueens[0][0] = 6;
        blackQueens[0][1] = 0;
        blackQueens[1][0] = 9;
        blackQueens[1][1] = 3;
        blackQueens[2][0] = 9;
        blackQueens[2][1] = 6;
        blackQueens[3][0] = 6;
        blackQueens[3][1] = 9;


        board[whiteQueens[0][0]][whiteQueens[0][1]] = WHITE_QUEEN;
        board[whiteQueens[1][0]][whiteQueens[1][1]] = WHITE_QUEEN;
        board[whiteQueens[2][0]][whiteQueens[2][1]] = WHITE_QUEEN;
        board[whiteQueens[3][0]][whiteQueens[3][1]] = WHITE_QUEEN;

        board[blackQueens[0][0]][blackQueens[0][1]] = BLACK_QUEEN;
        board[blackQueens[1][0]][blackQueens[1][1]] = BLACK_QUEEN;
        board[blackQueens[2][0]][blackQueens[2][1]] = BLACK_QUEEN;
        board[blackQueens[3][0]][blackQueens[3][1]] = BLACK_QUEEN;
    }

    /**
     * checks if board tile index is in bounds
     * @param row
     * @param col
     * @return
     */
    boolean inBounds(byte row, byte col) {
        return (row >= 0 && row < NUM_ROWS) && (col >= 0 && col < NUM_COLS);
    }

    /**
     * checks if position on board is free (not own by player pawn or arrow)
     * @param row
     * @param col
     * @return
     */
    boolean isPosFree(byte row, byte col) {
        return board[row][col] == FREE;
    }

    /**
     * checks if position is owned by arrow
     * @param row
     * @param col
     * @return
     */
    private boolean isArrow(byte row, byte col) { return  board[row][col] == ARROW; }

    /**
     * return queens for specified player
     * @param role player
     * @return
     */
    byte[][] getQueens(Role role) {
        if (role == Role.WHITE) {
            return whiteQueens;
        }
        return blackQueens;
    }

    /**
     * returns queen constant assigned to player
     * @param role player
     * @return
     */
    private byte getMarker(Role role) {
        if (role == Role.WHITE) {
            return WHITE_QUEEN;
        }
        return BLACK_QUEEN;
    }

    /**
     * changes players turn
     */
    private void changeCurrentPlayer() {
        if (currentPlayer == Role.WHITE) {
            currentPlayer = Role.BLACK;
        } else if (currentPlayer == Role.BLACK) {
            currentPlayer = Role.WHITE;
        }
    }


    @Override
    public Board makeMove(Move move) {
        byte[][] queens = getQueens(move.role);
        byte queenMarker = getMarker(move.role);

        for (byte[] queen : queens) {
            if (queen[0] == move.qr && queen[1] == move.qc) {
                queen[0] = move.qfr;
                queen[1] = move.qfc;
                break;
            }
        }
        board[move.qr][move.qc] = FREE;
        board[move.qfr][move.qfc] = queenMarker;
        board[move.ar][move.ac] = ARROW;

        markedPositions++;
        changeCurrentPlayer();


        return this;
    }

    @Override
    public Board undoMove(Move move) {
        byte[][] queens = getQueens(move.role);
        byte queenMarker = getMarker(move.role);

        for (byte[] queen : queens) {
            if (queen[0] == move.qfr && queen[1] == move.qfc) {
                queen[0] = move.qr;
                queen[1] = move.qc;
                break;
            }
        }
        board[move.qfr][move.qfc] = FREE;
        board[move.ar][move.ac] = FREE;
        board[move.qr][move.qc] = queenMarker;

        markedPositions--;
        changeCurrentPlayer();
        return this;
    }

    @Override
    public int evaluate(Role role) {
        return evaluationFunction.evaluate(this, role);
    }

    @Override
    public ArrayList<Move> actions(Role player) {
        return succesorFunction.generateSuccessorActions(this, player);
    }

    @Override
    public Board clone() {
        Board newBoard = new Board(heuristicOption);
        newBoard.currentPlayer = this.currentPlayer;
        newBoard.markedPositions = this.markedPositions;

        for (byte row = 0; row < board.length; row++) {
            newBoard.board[row] = Arrays.copyOf(board[row], board[row].length);
        }
        for (byte queen = 0; queen < whiteQueens.length; queen++) {
            newBoard.whiteQueens[queen] = Arrays.copyOf(whiteQueens[queen], whiteQueens[queen].length);
            newBoard.blackQueens[queen] = Arrays.copyOf(blackQueens[queen], blackQueens[queen].length);
        }

//        System.out.println("Cloned board");
//        printBoard();

        return newBoard;
    }

    @Override
    public Role getCurrentPlayer() {
        return currentPlayer;
    }

    @Override
    public boolean isMaxPlayerTurn() {

        return false;
    }

    /**
     * prints board in terminal
     */
    void printBoard() {

        System.out.println();
        System.out.println("/\t0\t1\t2\t3\t4\t5\t6\t7\t8\t9");
        for (byte r = 0; r < NUM_ROWS; ++r) {
            System.out.print(String.valueOf(r) + '\t');
            for (byte c = 0; c < NUM_COLS; ++c) {
                switch (board[r][c]) {
                    case FREE:
                        System.out.print("-\t");
                        break;
                    case WHITE_QUEEN:
                        System.out.print("W\t");
                        break;
                    case BLACK_QUEEN:
                        System.out.print("B\t");
                        break;
                    case ARROW:
                        System.out.print("x\t");
                        break;
                    default:
                        System.out.print("O\t");
                }
            }
            System.out.println();
        }
    }

    @Override
    public boolean isGameOver() {
        return !succesorFunction.hasPlayerPossibleMoves(this, currentPlayer);
    }

    @Override
    public boolean equals(Object o) {
        boolean equal = false;
        if (o instanceof Board) {
            Board other = (Board) o;
            equal = Arrays.deepEquals(board, other.board);
            equal &= Arrays.deepEquals(whiteQueens, other.whiteQueens);
            equal &= Arrays.deepEquals(blackQueens, other.blackQueens);

        }
        return equal;
    }

    /**
     * checks if player queen is on position specified by row and column
     * @param role player
     * @param row
     * @param column
     * @return
     */
    public boolean isQueenOnPawn(Role role, byte row, byte column) {
        byte[][] queens = getQueens(role);

        for (byte[] queen : queens) {
            if (queen[0] == row && queen[1] == column) {
                return true;
            }
        }
        return false;
    }

    /**
     * returns possible moves for queen on position
     * @param row
     * @param column
     * @return
     */
    public ArrayList<byte[]> getQueenMoves(byte row, byte column) {
        return succesorFunction.getQueenMoves(this, new byte[]{row, column});
    }

    /**
     * returns possible arrow shoots for queen on position
     * @param qr queen row
     * @param qc queen column
     * @param qfr queen row after move
     * @param qfc queen column after move
     * @return
     */
    public ArrayList<byte[]> getQueenShoots(byte qr, byte qc, byte qfr, byte qfc) {
        return succesorFunction.getQueensShoots(this, new byte[]{qr,qc}, new byte[]{qfr,qfc});
    }

    /**
     *  returns positions owned by arrows
     * @return
     */
    public ArrayList<byte[]> getArrowPosition() {
        ArrayList<byte []> arrows = new ArrayList<>();
        for(byte r = 0; r<NUM_ROWS; r++) {
            for (byte c = 0; c < NUM_COLS; c++) {
                if(isArrow(r,c))
                    arrows.add(new byte []{r, c});
            }
        }
        return arrows;
    }
}
