package Logic;

public class Configuration {
    public enum GamePlayOption {
        PLAYER_VS_AI,
        AI_VS_AI
    }

    public enum HeuristicOption {
        MOBILITY,
        TERRITORY,
        TERRITORY_MOBILITY
    }

    public enum SearchOption {
        SEARCH_TIME_LIMIT,
        SEARCH_MAX_DEPTH
    }


    private HeuristicOption heuristicOption;
    private SearchOption searchOption;
    private long searchValue;
    private int numberOfChildForState;
    private GamePlayOption gamePlayOption;

    public Configuration() {

    }

    public void setHeuristicOption(HeuristicOption heuristicOption) {
        this.heuristicOption = heuristicOption;
    }

    public HeuristicOption getHeuristicOption() {
        return heuristicOption;
    }

    public SearchOption getSearchOption() {
        return searchOption;
    }

    public void setSearchOption(SearchOption searchOption) {
        this.searchOption = searchOption;
    }

    public long getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(long searchValue) {
        this.searchValue = searchValue;
    }


    public GamePlayOption getGamePlayOption() {
        return gamePlayOption;
    }

    public void setGamePlayOption(GamePlayOption gamePlayOption) {
        this.gamePlayOption = gamePlayOption;
    }

    public int getNumberOfChildForState() {
        return numberOfChildForState;
    }

    public void setNumberOfChildForState(int numberOfChildForState) {
        this.numberOfChildForState = numberOfChildForState;
    }
}
