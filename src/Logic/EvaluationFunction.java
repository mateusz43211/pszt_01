package Logic;

import Logic.Board;

/**
 * Interface evaluation functions
 */
public interface EvaluationFunction {

    /**
     * returns heuristic value for specified game state
     * @param board board which implements game state
     * @param role player
     * @return
     */
    int evaluate(Board board, Role role);
}
