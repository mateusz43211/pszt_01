package Logic;

import java.util.ArrayList;

public interface GameState {
    /**
     * returns game state with applied move
     * @param move
     * @return
     */
    GameState makeMove(Move move);

    /**
     * returns evaluation value for player
     * @param role player
     * @return
     */
    int evaluate(Role role);

    /**
     * returns previous game state before latest move
     * @param move
     * @return
     */
    GameState undoMove(Move move);

    /**
     * return possible moves for player
     * @param player
     * @return
     */
    ArrayList<Move> actions(Role player);

    /**
     * returns copy of game state
     * @return
     */
    GameState clone();

    /**
     * returns player with current turn
     * @return
     */
    Role getCurrentPlayer();

    boolean isMaxPlayerTurn();

    /**
     * returns true if one of players have no possible move
     * @return
     */
    boolean isGameOver();
}
