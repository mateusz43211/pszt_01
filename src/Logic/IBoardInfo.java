package Logic;

import java.util.ArrayList;

public interface IBoardInfo {
    byte [][] getWhiteQueens();
    byte [][] getBlackQueens();
    boolean isPositionByRoleOccupied(Role role, byte row, byte column);
    boolean isPossibleMoves(ArrayList<byte []> possibleMoves, byte row, byte column);
    ArrayList<byte []> getPossibleMovesForQueen(byte row, byte column);
    ArrayList<byte []> getPossibleArrowShootsForQueen(byte qr, byte qc, byte qfr, byte qfc);
    ArrayList<byte []> getArrowPosition();
}
