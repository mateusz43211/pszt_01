package Logic;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *  holds game state and manage them
 */
public class LogicHolder implements IBoardInfo {
    private final OnLogicListener listener;
    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private SearchStrategy searchStrategy;
    private Board board;

    /**
     * creates logic holder
     * @param listener class which listen to logic callbacks
     * @param configuration configuration specified by user
     */
    public LogicHolder(OnLogicListener listener, Configuration configuration) {
        board = new Board(configuration.getHeuristicOption());
        this.listener = listener;

        switch (configuration.getSearchOption()){
            case SEARCH_TIME_LIMIT:
                searchStrategy = new SearchIterativeDeepening(configuration.getSearchValue(),
                        configuration.getNumberOfChildForState());

                break;
            case SEARCH_MAX_DEPTH:
                searchStrategy = new SearchMaxDepth((int) configuration.getSearchValue(),
                        configuration.getNumberOfChildForState());
                break;
        }
    }

    public Future<Move> searchNextMove() {
        return executor.submit(() -> {
            // board.printBoard();
            return searchStrategy.searchNextMove(board);
        });
    }

    /**
     * starts game AI vs AI, searching runs in separate thread to not block ui thread
     */
    public void startAIGame() {
        Thread thread = new Thread(()->{
            int markedPositions = 0;
            long startTime = System.nanoTime();

            while (!board.isGameOver()) {
                System.out.println("Next state");
                board.printBoard();
                Future<Move> nextMove = searchNextMove();
                try {
                    Move m = nextMove.get();
                    if (m == null) {
                        break;
                    }
                    board.makeMove(m);
                    listener.onPawnMove(m);
                    nextMove.cancel(true);

                    markedPositions++;
                    System.out.println("Marked positions" + markedPositions);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }

            listener.onGameOver(board.getCurrentPlayer().other());

            System.out.println("Game over!!! Winner player:" + board.getCurrentPlayer().other().name());
            long estimatedTime = (System.nanoTime() - startTime) / 1000000000;
            System.out.println("Time game elapsed: " + estimatedTime);
        });

        thread.start();

    }

    /**
     * Changes game state with specified move
     * @param qr queen row
     * @param qc queen column
     * @param qfr queen next position row
     * @param qfc queen next position column
     * @param ar arrow row
     * @param ac arrow column
     */
    public void makeMove(byte qr, byte qc, byte qfr, byte qfc, byte ar, byte ac) {
        long startTime = System.nanoTime();

        board.makeMove(new Move(Role.WHITE, qr, qc, qfr, qfc, ar, ac));

        if (!board.isGameOver()) {
            Future<Move> nextMove = searchNextMove();
            try {
                Move m = nextMove.get();
                board.makeMove(m);
                listener.onPawnMove(m);

                if (board.isGameOver()) {
                    listener.onGameOver(board.getCurrentPlayer().other());
                }

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        else {
            listener.onGameOver(board.getCurrentPlayer().other());
        }


        long estimatedTime = (System.nanoTime() - startTime) / 1000000000;

    }

    @Override
    public byte[][] getWhiteQueens() {
        return board.getQueens(Role.WHITE);
    }

    @Override
    public byte[][] getBlackQueens() {
        return board.getQueens(Role.BLACK);
    }

    @Override
    public boolean isPositionByRoleOccupied(Role role, byte row, byte column) {
        return board.isQueenOnPawn(role, row, column);
    }

    @Override
    public boolean isPossibleMoves(ArrayList<byte[]> possibleMoves, byte row, byte column) {
        for (byte[] pos : possibleMoves)
            if (pos[0] == row && pos[1] == column)
                return true;
        return false;
    }

    @Override
    public ArrayList<byte[]> getPossibleMovesForQueen(byte row, byte column) {
        return board.getQueenMoves(row, column);
    }

    @Override
    public ArrayList<byte[]> getPossibleArrowShootsForQueen(byte qr, byte qc, byte qfr, byte qfc) {
        return board.getQueenShoots(qr,qc,qfr,qfc);
    }

    @Override
    public ArrayList<byte []> getArrowPosition() {
        return board.getArrowPosition();
    }


}
