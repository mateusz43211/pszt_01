package Logic;

import java.util.BitSet;

import static Logic.Board.NUM_COLS;
import static Logic.Board.NUM_ROWS;

/**
 * A mobility (evaluation function.
 *
 */
public class MobilityEvaluationFunction implements EvaluationFunction {

    private BitSet[] ourMobility, oppMobility;
    private Board board;

    /**
     * Constructor
     */
    public MobilityEvaluationFunction() {
        ourMobility = new BitSet[4];
        oppMobility = new BitSet[4];
        for (byte i = 0; i < 4; i++) {
            ourMobility[i] = new BitSet();
            oppMobility[i] = new BitSet();
        }
    }

    /**
     * Function return value of heuristic
     * @param board the game board
     * @param role the game role (white or black)
     * @return value of heuristic
     */
    @Override
    public int evaluate(Board board, Role role) {

        this.board = board;

        int eval = mobilityEvaluation(role);
        clear();
        return eval;
    }

    /**
     * Function calculate mobility evaluation function
     * @param role the game role (white or black)
     * @return value of evaluation function
     */
    private int mobilityEvaluation(Role role) {
        initMobilityWebs(role, ourMobility);
        initMobilityWebs(role.other(), oppMobility);

        int ourPoints = 0;
        int oppPoints = 0;
        byte rowMajorIndex;
        byte ourMob = 0;
        byte oppMob = 0;

        for (byte r = 0; r < NUM_ROWS; r++) {
            for (byte c = 0; c < NUM_COLS; c++) {

                rowMajorIndex = (byte) (r * NUM_ROWS + c);

                for (byte i = 0; i < 4; i++) {
                    if (ourMobility[i].get(rowMajorIndex)) {
                        ourMob++;
                    }
                    if (oppMobility[i].get(rowMajorIndex)) {
                        oppMob++;
                    }
                }
            }
        }

        ourPoints += ourMob;
        oppPoints += oppMob;
        return ourPoints - oppPoints;
    }

    /**
     * Function init mobility set
     * @param role the game role (white or black)
     * @param mobility the player mobility
     */
    private void initMobilityWebs(Role role, BitSet[] mobility) {
        byte[][] queens = board.getQueens(role);
        byte[] queen;
        byte r, c;

        for (byte n = 0; n < 4; n++) {
            queen = queens[n];
            for (byte[] dir : Board.dirs) {
                r = (byte) (queen[0] + dir[0]);
                c = (byte) (queen[1] + dir[1]);

                while (board.inBounds(r, c) && board.isPosFree(r, c)) {
                    mobility[n].set(r * NUM_ROWS + c);
                    r += dir[0];
                    c += dir[1];
                }
            }
        }
    }

    /**
     * Function clear players mobility set
     */
    private void clear() {
        for (byte i = 0; i < 4; i++)
        {
            ourMobility[i].clear();
            oppMobility[i].clear();
        }
        board = null;
    }
}
