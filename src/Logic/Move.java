package Logic;

/**
 * holds values for player move
 */
public class Move implements Comparable<Move>{

    public Role role;
    public byte qr, qc, qfr, qfc, ar, ac;
    public int v;

    public Move(Role role, byte qr, byte qc, byte qfr, byte qfc, byte ar, byte ac) {
        this.role = role;
        this.qr = qr;
        this.qc = qc;
        this.qfr = qfr;
        this.qfc = qfc;
        this.ar = ar;
        this.ac = ac;
    }

    @Override
    public int compareTo(Move o) {

        if (v > o.v)
        {
            return -1;
        }
        if (o.v > v)
        {
            return 1;
        }
        return 0;
    }
}
