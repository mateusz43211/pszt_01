package Logic;

/**
 * Interface Logic Lister transfer information about move with logic to gui and about game over
 */
public interface OnLogicListener {
    void onPawnMove(Move move);
    void onGameOver(Role role);
}
