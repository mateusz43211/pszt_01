package Logic;

/**
 * enumeration for players
 */

public enum Role {
    WHITE("W"),
    BLACK("B");

    private String value;

    private Role(String value) {
        this.value = value;
    }

    public Role other() {
        if (this == WHITE){
            return BLACK;
        }
        else if (this == BLACK){
            return WHITE;
        }
        return null;
    }
}
