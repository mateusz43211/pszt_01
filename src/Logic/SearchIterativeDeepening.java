package Logic;

import java.util.ArrayList;
import java.util.List;

/**
 * controls iterative deepening search of game states in specified time bounds
 */
public class SearchIterativeDeepening implements SearchStrategy {

    private final long SEARCH_TIME_LIMIT;
    private Role maxPlayer = Role.WHITE;
    private long searchEndTime;
    private boolean searchCutoff = false;
    private final int NUMBER_OF_ITEMS;

    SearchIterativeDeepening(long searchTimeLimit, int numberOfItems) {
        this.SEARCH_TIME_LIMIT = searchTimeLimit;
        this.NUMBER_OF_ITEMS = numberOfItems;
    }

    @Override
    public Move searchNextMove(GameState state) {
        long startTime = System.nanoTime();
        searchEndTime = startTime + SEARCH_TIME_LIMIT;

        int maxScore = Integer.MIN_VALUE;

        Role currentPlayer = state.getCurrentPlayer();

        maxPlayer = currentPlayer;

        ArrayList<Move> moves = state.actions(currentPlayer);

        List<Move> bestMoves = Utils.getBestMovesForPlayer(
                currentPlayer == maxPlayer, moves, NUMBER_OF_ITEMS);
        GameState childState = state.clone();

        Move bestMove = bestMoves.get(0);

        for (Move move : bestMoves) {
            childState.makeMove(move);

            // set possible search time for each branch of game tree
            long searchTimeLimit = ((SEARCH_TIME_LIMIT - 1000000000) / (bestMoves.size()));
            int score = iterativeDeepeningSearch(childState, searchTimeLimit);

            if (score > maxScore) {
                maxScore = score;
                bestMove = move;
            }

            childState.undoMove(move);
        }

        long estimatedTime = (System.nanoTime() - startTime) / 1000000000;

        System.out.println("Time searchNextMove elapsed: " + estimatedTime);

        return bestMove;
    }

    private int iterativeDeepeningSearch(GameState state, long searchTimeLimit) {
        long startTime = System.nanoTime();
        long endTime = startTime + searchTimeLimit;
        int depth = 1;
        int score = Integer.MIN_VALUE;

        while (true) {
            long currentTime = System.nanoTime();

            if (currentTime >= endTime) {
                break;
            }

            int searchResult = alphaBeta(state, depth, Integer.MIN_VALUE, Integer.MAX_VALUE, currentTime, endTime-currentTime);

            score = searchResult;


            depth++;
        }

        return score;
    }

    int alphaBeta(GameState state, int depthRemaining, int alpha, int beta, long startTime, long timeLeft) {
        Role currentPlayer = state.getCurrentPlayer();

        long currentTime = System.nanoTime();

        if (depthRemaining == 0 || currentTime - startTime >  timeLeft) {
            return state.evaluate(currentPlayer);
        }

        ArrayList<Move> possibleMoves = state.actions(currentPlayer);

        List<Move> bestMoves = Utils.getBestMovesForPlayer(
                currentPlayer == maxPlayer, possibleMoves, NUMBER_OF_ITEMS);


        GameState child = state.clone();

        for (Move move : bestMoves) {
            child.makeMove(move);

            // for debug purposes
//            Board b = (Board) child;
//            b.printBoard();

            int score = alphaBeta(child, depthRemaining - 1, alpha, beta, startTime, timeLeft);

            if (state.getCurrentPlayer() == maxPlayer) {
                alpha = Math.max(alpha, score);
                if (alpha >= beta) {
                    break;
                }
            } else {
                beta = Math.min(beta, score);
                if (beta <= alpha) {
                    break;
                }
            }
            child.undoMove(move);
        }

        return (currentPlayer == maxPlayer) ? alpha : beta;
    }


}
