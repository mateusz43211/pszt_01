package Logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * controls search of game states with maximal depth
 */
public class SearchMaxDepth implements SearchStrategy {

    private Role minPlayer = Role.BLACK;
    private Role maxPlayer = Role.WHITE;
    private final int NUMBER_OF_ITEMS;
    private int processedChilds = 0;
    private final int MAX_DEPTH;

    public SearchMaxDepth(int maxDepth, int maxItems) {
        this.MAX_DEPTH = maxDepth;
        this.NUMBER_OF_ITEMS = maxItems;
    }

    @Override
    public Move searchNextMove(GameState state) {
        long startTime = System.nanoTime();

        int maxScore = Integer.MIN_VALUE;

        Role currentPlayer = state.getCurrentPlayer();

        maxPlayer = currentPlayer;
        minPlayer = currentPlayer.other();

        ArrayList<Move> moves = state.actions(currentPlayer);

        // gets best moves for current player
        List<Move> bestMoves = Utils.getBestMovesForPlayer(
                currentPlayer == maxPlayer, moves, NUMBER_OF_ITEMS);
        GameState childState = state.clone();

        Move bestMove = bestMoves.size() != 0 ? bestMoves.get(0) : null;

        for (Move move : bestMoves) {
            childState.makeMove(move);

            // searchNextMove
            int score = alphaBeta(childState, MAX_DEPTH, Integer.MIN_VALUE, Integer.MAX_VALUE);

            if (score > maxScore) {
                maxScore = score;
                bestMove = move;
            }

            childState.undoMove(move);
        }

        long estimatedTime = (System.nanoTime() - startTime) / 1000000000;

        System.out.println("Time searchNextMove elapsed: " + estimatedTime);

        return bestMove;
    }

    int alphaBeta(GameState state, int depthRemaining, int alpha, int beta) {
        Role currentPlayer = state.getCurrentPlayer();
        if (depthRemaining == 0) {
            processedChilds++;
            return state.evaluate(currentPlayer);
        }

        ArrayList<Move> possibleMoves = state.actions(currentPlayer);

        List<Move> bestMoves = Utils.getBestMovesForPlayer(
                currentPlayer == maxPlayer, possibleMoves, NUMBER_OF_ITEMS);


        GameState child = state.clone();

        for (Move move : bestMoves) {
            child.makeMove(move);

            // for debug purposes
//            Board b = (Board) child;
//            b.printBoard();

            int score = alphaBeta(child, depthRemaining - 1, alpha, beta);

            if (state.getCurrentPlayer() == maxPlayer) {
                alpha = Math.max(alpha, score);
                if (alpha >= beta) {
                    break;
                }
            } else {
                beta = Math.min(beta, score);
                if (beta <= alpha) {
                    break;
                }
            }
            child.undoMove(move);
        }

        return (currentPlayer == maxPlayer) ? alpha : beta;
    }
}
