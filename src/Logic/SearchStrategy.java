package Logic;

public interface SearchStrategy {

    /**
     * returns best move for specified game state
     * @param gameState
     * @return
     */
    Move searchNextMove(GameState gameState);
}
