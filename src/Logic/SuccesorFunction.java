package Logic;

import java.util.*;

/**
 * generates next moves for game state
 */

class SuccesorFunction {
    private Board board;
    private Role role;
    private PriorityQueue<Move> actions = new PriorityQueue<>();


    /**
     * generates next moves for game state
     * @param board hold game state
     * @param role player
     * @return
     */
    ArrayList<Move> generateSuccessorActions(Board board, Role role) {
        this.board = board.clone();
        this.role = role;

        // board.printBoard();

        final byte[][] queens = board.getQueens(role);

        for (final byte[] queen : queens) {
            for (byte[] dir : Board.dirs) {
                calculateQueenMoves(queen, dir[0], dir[1]);
            }
        }
        ArrayList<Move> toReturn = new ArrayList<>();

        toReturn.addAll(actions);

        clear();

        return toReturn;
    }

    private void clear() {
        actions.clear();
        this.role = null;
        this.board = null;
    }

    /**
     * calculates possible moves of queens in specified direction
     * @param queen queen positions
     * @param dr direction increment in rows
     * @param dc direction increment in columns
     */
    private void calculateQueenMoves(byte[] queen, byte dr, byte dc) {
        byte row = (byte) (queen[0] + dr);
        byte col = (byte) (queen[1] + dc);

        // board.printBoard();
        while (board.inBounds(row, col) && board.isPosFree(row, col)) {

            for (byte[] dir : Board.dirs) {
                calculateArrowShots(queen, row, col, dir[0], dir[1]);
            }
            row += dr;
            col += dc;
        }
    }

    /**
     * calculates possible queen arrow shots in specified direction
     * @param queen queen positions
     * @param qfr queen next row
     * @param qfc queen next column
     * @param dr arrow direction increment in rows
     * @param dc arrow direction increment in columns
     */
    private void calculateArrowShots(byte[] queen, byte qfr, byte qfc, byte dr, byte dc) {
        byte row = (byte) (qfr + dr);
        byte col = (byte) (qfc + dc);

        while (board.inBounds(row, col) && (board.isPosFree(row, col) || (row == queen[0] && col == queen[1]))) {
            // evaluate the actions v value for comparison purposes.  This way the searchNextMove evaluates better moves first.
            Move move = new Move(role, queen[0], queen[1], qfr, qfc, row, col);
//            System.out.println("Before move:");
//            board.printBoard();
            board.makeMove(move);
//            System.out.println("After move:");
//            board.printBoard();

            move.v = board.evaluate(role);
            board.undoMove(move);
//            System.out.println("After undo move:");
//            board.printBoard();

            actions.add(move);
            row += dr;
            col += dc;
        }

    }

    /**
     * checks if player have possible moves
     * @param board game state
     * @param role player
     * @return
     */
    boolean hasPlayerPossibleMoves(Board board, Role role) {
        this.board = board;
        this.role = role;

        for (byte[] queen : board.getQueens(role)) {
            for (byte[] dir : Board.dirs) {
                calculateQueenMoves(queen, dir[0], dir[1]);
                if (!actions.isEmpty()) {
                    clear();
                    return true;
                }
            }
        }
        clear();
        return false;
    }

    /**
     * returns possible moves for queens
     * @param board game state
     * @param queen player
     * @return
     */
    ArrayList<byte []> getQueenMoves(Board board, byte[] queen) {
        this.board = board;

        ArrayList<byte []> moves = new ArrayList<>();
        byte dr, dc;

        for (byte[] dir : Board.dirs) {

            dr = dir[0];
            dc = dir[1];

            byte row = (byte) (queen[0] + dr);
            byte col = (byte) (queen[1] + dc);

            // board.printBoard();
            while (board.inBounds(row, col) && board.isPosFree(row, col)) {
                moves.add(new byte []{row, col});
                row += dr;
                col += dc;
            }
        }

        return moves;
    }

    /**
     * return possible arrow shots for queen
     * @param board game state
     * @param queen queen position
     * @param newQueenPosition queen next position
     * @return
     */
    ArrayList<byte []> getQueensShoots(Board board, byte[] queen, byte[] newQueenPosition) {
        ArrayList<byte []> moves = new ArrayList<>();
        byte dr, dc;
        for (byte[] dir : Board.dirs) {

            dr = dir[0];
            dc = dir[1];

            byte row = (byte) (newQueenPosition[0] + dr);
            byte col = (byte) (newQueenPosition[1] + dc);

            while (board.inBounds(row, col) && (board.isPosFree(row, col) || (row == queen[0] && col == queen[1]))) {
                moves.add(new byte []{row, col});
                row += dr;
                col += dc;
            }
        }

        return moves;
    }
}
