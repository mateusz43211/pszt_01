package Logic;

import java.util.Arrays;

import static Logic.Board.NUM_COLS;
import static Logic.Board.NUM_ROWS;

/**
 * A territory (evaluation function.
 * A territory function is used in limited version (maximum free moves).
 *
 * @author Karol
 */
public class TerritoryEvaluationFunction implements EvaluationFunction {

    private Board board;
    private byte[][] ourTerritory, oppTerritory;

    /**
     * Constructor
     */
    public TerritoryEvaluationFunction() {
        ourTerritory = new byte[NUM_ROWS][NUM_COLS];
        oppTerritory = new byte[NUM_ROWS][NUM_COLS];
        clear();
    }

    /**
     * Function return value of heuristic
     * @param board the game board
     * @param role the game role (white or black)
     * @return value of heuristic
     */
    @Override
    public int evaluate(Board board, Role role) {
        this.board = board;
        int eval = territoryEvaluation(role);
        clear();
        return eval;
    }

    /**
     * Function calculate territory evaluation function
     * @param role the game role (white or black)
     * @return value of evaluation function
     */
    private int territoryEvaluation(Role role) {
        initTerritoryWebs(role, ourTerritory);
        initTerritoryWebs(role.other(), oppTerritory);
        int ourPoints = 0;
        int oppPoints = 0;
        for (byte row = 0; row < NUM_ROWS; row++) {
            for (byte col = 0; col < NUM_COLS; col++) {
                if (ourTerritory[row][col] < oppTerritory[row][col]) {
                    ourPoints++;
                } else if (ourTerritory[row][col] > oppTerritory[row][col]) {
                    oppPoints++;
                }
            }
        }
        return ourPoints - oppPoints;
    }

    /**
     * Function calculate value of territory
     * @param role the game role (white or black)
     * @param territory the player territory
     */
    private void initTerritoryWebs(Role role, byte[][] territory) {
        for (byte[] queen : board.getQueens(role))
            for (byte[] dir : board.dirs)
                checkTerritory(territory, (byte)(queen[0]+dir[0]), (byte)(queen[1]+dir[1]), (byte) 1);
    }

    /**
     * Recursive function help calculating value of territory
     * @param territory the player territory
     * @param r actual row
     * @param c actual column
     * @param move number of moves
     */
    private void checkTerritory(byte[][] territory, byte r, byte c, byte move) {
        if(board.inBounds(r, c) && board.isPosFree(r, c) && territory[r][c] > move)
        {
            territory[r][c] = move;
            if(move<=3) {
                for (byte[] dir : board.dirs) {
                    checkTerritory(territory, (byte) (r + dir[0]), (byte) (c + dir[1]), (byte) (1 + move));
                }
            }
        }
    }

    /**
     * Function clear players territory (set value of territory on maximum value)
     */
    private void clear(){
        for (byte i = 0; i < NUM_ROWS; i++)
        {
            Arrays.fill(ourTerritory[i], Byte.MAX_VALUE);
            Arrays.fill(oppTerritory[i], Byte.MAX_VALUE);
        }
        board = null;
    }

}
