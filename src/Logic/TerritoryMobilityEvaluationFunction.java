package Logic;

import java.util.Arrays;
import java.util.BitSet;

import static Logic.Board.NUM_COLS;
import static Logic.Board.NUM_ROWS;

/**
 * A terrritory-mobility (evaluation function.
 * A territory function is used in limited version (maximum free moves).
 *
 * @author Karol
 */
public class TerritoryMobilityEvaluationFunction implements EvaluationFunction {

    private Board board;
    private byte[][] ourTerritory, oppTerritory;
    private BitSet[] ourMobility, oppMobility;

    /**
     * Constructor
     */
    public TerritoryMobilityEvaluationFunction() {
        ourTerritory = new byte[NUM_ROWS][NUM_COLS];
        oppTerritory = new byte[NUM_ROWS][NUM_COLS];
        ourMobility = new BitSet[4];
        oppMobility = new BitSet[4];
        for (byte i = 0; i < 4; i++) {
            ourMobility[i] = new BitSet();
            oppMobility[i] = new BitSet();
        }
        clear();
    }

    /**
     * Function return value of heuristic
     * @param board the game board
     * @param role the game role (white or black)
     * @return value of heuristic
     */
    @Override
    public int evaluate(Board board, Role role) {
        this.board = board;
        int eval = territoryMobilityEvaluation(role);
        clear();
        return eval;
    }

    /**
     * Function calculate territory-mobility evaluation function
     * @param role the game role (white or black)
     * @return value of evaluation function
     */
    private int territoryMobilityEvaluation(Role role) {
        initTerritoryWebs(role,ourTerritory);
        initTerritoryWebs(role.other(),oppTerritory);
        initMobilityWebs(role, ourMobility);
        initMobilityWebs(role.other(), oppMobility);

        int ourPoints = 0;
        int oppPoints = 0;
        byte rowMajorIndex;
        byte ourMob;
        byte oppMob;

        for (byte row = 0; row < NUM_ROWS; row++) {
            for (byte col = 0; col < NUM_COLS; col++) {
                ourMob = 0;
                oppMob = 0;
                rowMajorIndex = (byte) (row * NUM_ROWS + col);
                for (byte i = 0; i < 4; i++) {
                    if (ourMobility[i].get(rowMajorIndex)) {
                        ourMob++;
                    }
                    if (oppMobility[i].get(rowMajorIndex)) {
                        oppMob++;
                    }
                }
                if(ourTerritory[row][col] < oppTerritory[row][col]) {
                    ourPoints+= 4 + ourMob - oppMob;
                }
                else if (ourTerritory[row][col] > oppTerritory[row][col]) {
                    oppPoints+= 4 + oppMob - ourMob;
                }
                else {
                    ourPoints += ourMob;
                    oppPoints += oppMob;
                }
            }
        }
        return ourPoints-oppPoints;
    }

    /**
     * Function calculate value of territory
     * @param role the game role (white or black)
     * @param territory the player territory
     */
    private void initTerritoryWebs(Role role, byte[][] territory) {
        for (byte[] queen : board.getQueens(role))
            for (byte[] dir : board.dirs)
                checkTerritory(territory, (byte)(queen[0]+dir[0]), (byte)(queen[1]+dir[1]), (byte) 1);
    }

    /**
     * Recursive function help calculating value of territory
     * @param territory the player territory
     * @param r actual row
     * @param c actual column
     * @param move number of moves
     */
    private void checkTerritory(byte[][] territory, byte r, byte c, byte move) {
        if(board.inBounds(r, c) && board.isPosFree(r, c) && territory[r][c] > move)
        {
            territory[r][c] = move;
            if(move<=3) {
                for (byte[] dir : board.dirs) {
                    checkTerritory(territory, (byte) (r + dir[0]), (byte) (c + dir[1]), (byte) (1 + move));
                }
            }
        }
    }

    /**
     * Function init mobility set
     * @param role the game role (white or black)
     * @param mobility the player mobility
     */
    private void initMobilityWebs(Role role, BitSet[] mobility) {
        byte[][] queens = board.getQueens(role);
        byte[] queen;
        byte r, c;

        for (byte n = 0; n < 4; n++) {
            queen = queens[n];
            for (byte[] dir : Board.dirs) {
                r = (byte) (queen[0] + dir[0]);
                c = (byte) (queen[1] + dir[1]);

                while (board.inBounds(r, c) && board.isPosFree(r, c)) {
                    mobility[n].set(r * NUM_ROWS + c);
                    r += dir[0];
                    c += dir[1];
                }
            }
        }
    }

    /**
     * Function clear players mobility set and territory (set value of territory on maximum value)
     */
    private void clear() {
        for (byte i = 0; i < NUM_ROWS; i++)
        {
            Arrays.fill(ourTerritory[i], Byte.MAX_VALUE);
            Arrays.fill(oppTerritory[i], Byte.MAX_VALUE);
        }
        for (byte i = 0; i < 4; i++)
        {
            ourMobility[i].clear();
            oppMobility[i].clear();
        }
        board = null;
    }
}