package Logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Utils {
    /**
     * returns sorted best moves for player
     * @param isMaxPlayerTurn value to specify if to take minimal or maximal values in possible moves
     * @param possibleMoves all possible moves
     * @param numberOfItems number of items to return
     * @return
     */
    public static List<Move> getBestMovesForPlayer(boolean isMaxPlayerTurn, ArrayList<Move> possibleMoves, int numberOfItems) {
        List<Move> bestMoves;
        if (possibleMoves.size() > numberOfItems) {
            if (isMaxPlayerTurn) {
                Collections.sort(possibleMoves);
                bestMoves = possibleMoves.subList(0, numberOfItems);
            } else {
                Collections.sort(possibleMoves, Collections.reverseOrder());
                bestMoves = possibleMoves.subList(0, numberOfItems);
            }
        } else {
            bestMoves = possibleMoves;
        }
        return bestMoves;
    }
}
